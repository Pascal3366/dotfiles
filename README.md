# dotfiles
My dotfiles from 2017, use stow to install

## Herbstluftwm
## Polybar
## Ranger
## Ncmpcpp + Mpd
## Terminator
## Zsh + Oh-my-zsh
## Vim
## Xresources (including rofi theme)
