#!/bin/bash
RANDOMNUMBER=`od -vAn -N4 -tu4 < /dev/urandom`
Date=`date "+%b_%d_%Y"`
Timestamp=`date "+%s"`
terminator -e 'recordmydesktop -o "$HOME/record-$Date-$Timestamp-$RANDOMNUMBER"'
