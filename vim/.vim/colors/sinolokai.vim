set background=dark
highlight clear

if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "sinolokai"

hi Cursor ctermfg=235 ctermbg=231 cterm=NONE guifg=#262626 guibg=#FFFFFF gui=NONE
hi Visual guifg=NONE guibg=#444444 guisp=#444444 gui=NONE ctermfg=NONE ctermbg=238 cterm=NONE
hi CursorLine guifg=NONE guibg=#303030 guisp=#303030 gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi CursorColumn guifg=NONE guibg=#303030 guisp=#303030 gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE

"
hi ColorColumn ctermfg=NONE ctermbg=237 cterm=NONE guifg=NONE guibg=#3A3A3A gui=NONE
"

hi LineNr cterm=none ctermbg=233 ctermfg=242 gui=none guibg=#121212 guifg=#6C6C6C
hi VertSplit ctermfg=236 ctermbg=235 cterm=NONE guifg=#303030 guibg=#262626 gui=NONE
hi MatchParen guifg=NONE guibg=#FF8700 guisp=#FF8700 gui=bold ctermfg=NONE ctermbg=208 cterm=bold
hi StatusLine guifg=#585858 guibg=#FFFFD7 guisp=#F8F8F2 gui=NONE ctermfg=240 ctermbg=230 cterm=NONE
hi StatusLineNC guifg=#4D4D4D guibg=#080808 guisp=NONE gui=NONE ctermfg=8 ctermbg=232 cterm=NONE
hi PMenu guifg=#5FD7FF guibg=NONE guisp=NONE gui=NONE ctermfg=81 ctermbg=NONE cterm=NONE
hi PMenuThumb guifg=#5FD7FF guibg=NONE guisp=NONE gui=NONE ctermfg=81 ctermbg=NONE cterm=NONE
hi PMenuSbar guifg=NONE guibg=#080808 guisp=NONE gui=NONE ctermfg=NONE ctermbg=232 cterm=NONE
hi PMenuSel guifg=NONE guibg=#4D4D4D guisp=NONE gui=NONE ctermfg=NONE ctermbg=8 cterm=NONE
hi IncSearch guifg=#D7D787 guibg=NONE guisp=NONE gui=NONE ctermfg=186 ctermbg=NONE cterm=NONE
hi Search guifg=#A8A8A8 guibg=#808080 guisp=NONE gui=NONE ctermfg=15 ctermbg=234 cterm=NONE
hi Directory guifg=#87D700 guibg=NONE guisp=NONE gui=bold ctermfg=112 ctermbg=NONE cterm=bold
hi Folded guifg=#585858 guibg=NONE guisp=NONE gui=NONE ctermfg=240 ctermbg=NONE cterm=NONE

"
hi TabLineFill term=bold cterm=bold ctermbg=0
"

hi Normal ctermfg=231 ctermbg=234 cterm=NONE guifg=#FFFFFF guibg=#1C1C1C gui=NONE
hi Boolean ctermfg=98 ctermbg=NONE cterm=NONE guifg=#875FD7 guibg=NONE gui=NONE
hi Character guifg=#D7D75F guibg=NONE guisp=NONE gui=NONE ctermfg=185 ctermbg=NONE cterm=NONE
hi Comment ctermfg=101 ctermbg=NONE cterm=italic guifg=#87875F guibg=NONE gui=italic
hi Conditional guifg=#FF005F guibg=NONE guisp=NONE gui=bold ctermfg=197 ctermbg=NONE cterm=bold
hi Constant guifg=#AF87FF guibg=NONE guisp=NONE gui=NONE ctermfg=141 ctermbg=NONE cterm=none
hi Define ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi DiffAdd guifg=NONE guibg=#005F5F guisp=NONE gui=NONE ctermfg=NONE ctermbg=23 cterm=NONE
hi DiffChange guifg=#8A8A8A guibg=#4E4E4E guisp=NONE gui=NONE ctermfg=245 ctermbg=239 cterm=NONE
hi DiffText guifg=NONE guibg=#4E4E4E guisp=NONE gui=bold ctermfg=NONE ctermbg=239 cterm=bold
hi DiffDelete guifg=#87005F guibg=#5F005F guisp=NONE gui=NONE ctermfg=89 ctermbg=53 cterm=NONE
hi ErrorMsg ctermfg=231 ctermbg=197 cterm=bold,italic guifg=#FFFFFF guibg=#FF005F gui=bold,italic
hi Error guifg=#87005F guibg=#5F005F guisp=NONE gui=NONE ctermfg=89 ctermbg=53 cterm=NONE
hi WarningMsg ctermfg=231 ctermbg=197 cterm=bold,italic guifg=#FFFFFF guibg=#FF005F gui=bold,italic
hi Float guifg=#AF87FF guibg=NONE guisp=NONE gui=NONE ctermfg=141 ctermbg=NONE cterm=NONE
hi Function guifg=NONE guibg=#AFD700 guisp=NONE gui=NONE ctermfg=148 ctermbg=NONE cterm=NONE
hi Identifier guifg=#D7D75F guibg=NONE guisp=NONE gui=NONE ctermfg=185 ctermbg=NONE cterm=NONE
hi Keyword guifg=#FF005F guibg=NONE guisp=NONE gui=bold ctermfg=197 ctermbg=NONE cterm=bold
hi Label ctermfg=186 ctermbg=NONE cterm=NONE guifg=#D7D787 guibg=NONE gui=NONE
hi NonText ctermfg=238 ctermbg=NONE cterm=NONE guifg=#444444 guibg=NONE gui=NONE
hi Number ctermfg=141 ctermbg=NONE cterm=NONE guifg=#AF87FF guibg=NONE gui=NONE
hi Structure ctermfg=141 ctermbg=NONE cterm=NONE guifg=#AF87FF guibg=NONE gui=NONE
hi Operator ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi PreProc ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi Special ctermfg=231 ctermbg=NONE cterm=NONE guifg=#FFFFFF guibg=NONE gui=NONE
hi SpecialKey ctermfg=236 ctermbg=NONE cterm=NONE guifg=#303030 guibg=NONE gui=NONE
hi Statement ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi StorageClass guifg=#FF8700 guibg=NONE guisp=NONE gui=NONE ctermfg=208 ctermbg=NONE cterm=NONE
hi String ctermfg=185 ctermbg=NONE cterm=NONE guifg=#FFFF5F guibg=NONE gui=NONE
hi Tag guifg=#FF005F guibg=NONE guisp=NONE gui=NONE ctermfg=197 ctermbg=NONE cterm=NONE
hi Title guifg=#FF875F guibg=NONE guisp=NONE gui=NONE ctermfg=209 ctermbg=NONE cterm=NONE
hi Todo ctermfg=255 ctermbg=NONE cterm=bold guifg=#EEEEEE guibg=NONE gui=bold
hi Type guifg=197 guibg=NONE guisp=NONE gui=NONE ctermfg=197 ctermbg=NONE cterm=NONE
hi Underlined guifg=#FF0000 guibg=NONE guisp=NONE gui=underline ctermfg=8 ctermbg=NONE cterm=underline

hi rubyClass ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi rubyFunction ctermfg=148 ctermbg=NONE cterm=NONE guifg=#AFD700 guibg=NONE gui=NONE
hi rubyInterpolationDelimiter ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi rubySymbol ctermfg=141 ctermbg=NONE cterm=NONE guifg=#AF87FF guibg=NONE gui=NONE
hi rubyConstant ctermfg=81 ctermbg=NONE cterm=italic guifg=#5FD7FF guibg=NONE gui=italic
"hi rubyConstant ctermfg=15 ctermbg=NONE cterm=NONE guifg=#66d9ef guibg=NONE gui=NONE
hi rubyStringDelimiter ctermfg=15 ctermbg=NONE cterm=NONE guifg=#FFFFFF guibg=NONE gui=NONE
hi rubyBlockParameter ctermfg=81 ctermbg=NONE cterm=italic guifg=#5FD7FF guibg=NONE gui=italic
hi rubyInstanceVariable ctermfg=203 ctermbg=NONE cterm=NONE guifg=#FF5F5F guibg=NONE gui=NONE
hi rubyInclude ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi rubyGlobalVariable ctermfg=179 ctermbg=NONE cterm=NONE guifg=#D7AF5F guibg=NONE gui=NONE
hi rubyRegexp ctermfg=185 ctermbg=NONE cterm=NONE guifg=#D7D75F guibg=NONE gui=NONE
hi rubyRegexpDelimiter ctermfg=185 ctermbg=NONE cterm=NONE guifg=#D7D75F guibg=NONE gui=NONE
hi rubyEscape ctermfg=141 ctermbg=NONE cterm=NONE guifg=#AF87FF guibg=NONE gui=NONE
hi rubyControl ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi rubyClassVariable ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi rubyOperator ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi rubyException ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi rubyPseudoVariable ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE

hi rubyRailsUserClass ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi rubyRailsARAssociationMethod ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi rubyRailsARMethod ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi rubyRailsRenderMethod ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi rubyRailsMethod ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE

hi erubyDelimiter ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi erubyComment ctermfg=95 ctermbg=NONE cterm=NONE guifg=##875F5F guibg=NONE gui=NONE
hi erubyRailsMethod ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE

hi htmlH1 ctermfg=15 ctermbg=NONE cterm=NONE guifg=#FFFFFF guibg=NONE gui=NONE
hi htmlH2 ctermfg=15 ctermbg=NONE cterm=NONE guifg=#FFFFFF guibg=NONE gui=NONE
hi htmlH3 ctermfg=15 ctermbg=NONE cterm=NONE guifg=#FFFFFF guibg=NONE gui=NONE
hi htmlH4 ctermfg=15 ctermbg=NONE cterm=NONE guifg=#FFFFFF guibg=NONE gui=NONE
hi htmlH5 ctermfg=15 ctermbg=NONE cterm=NONE guifg=#FFFFFF guibg=NONE gui=NONE
hi htmlH6 ctermfg=15 ctermbg=NONE cterm=NONE guifg=#FFFFFF guibg=NONE gui=NONE
hi htmlTag ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi htmlEndTag ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi htmlTagName ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi htmlArg ctermfg=112 ctermbg=NONE cterm=NONE guifg=#87D700 guibg=NONE gui=NONE
hi htmlSpecialChar ctermfg=141 ctermbg=NONE cterm=NONE guifg=#282729 guibg=NONE gui=NONE
hi htmlLink ctermfg=15 ctermbg=NONE cterm=NONE guifg=#FFFFFF guibg=NONE gui=NONE
hi htmlInterpolation ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE

hi javaScriptFunction ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=italic
hi javaScriptRailsFunction ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi javaScriptBraces ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE

hi yamlKey ctermfg=197 ctermbg=NONE cterm=NONE guifg=#FF005F guibg=NONE gui=NONE
hi yamlAnchor ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi yamlAlias ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi yamlDocumentHeader ctermfg=186 ctermbg=NONE cterm=NONE guifg=#D7D787 guibg=NONE gui=NONE

hi cssURL ctermfg=208 ctermbg=NONE cterm=italic guifg=#FF8700 guibg=NONE gui=italic
hi cssFunctionName ctermfg=15 ctermbg=NONE cterm=NONE guifg=#FFFFFF guibg=NONE gui=NONE
hi cssColor ctermfg=141 ctermbg=NONE cterm=NONE guifg=#AF87FF guibg=NONE gui=NONE
hi cssPseudoClassId ctermfg=148 ctermbg=NONE cterm=NONE guifg=#AFD700 guibg=NONE gui=NONE
hi cssClassName ctermfg=154 ctermbg=NONE cterm=NONE guifg=#AFFF00 guibg=NONE gui=NONE
hi cssClass ctermfg=154 ctermbg=NONE cterm=NONE guifg=#AFFF00 guibg=NONE gui=NONE
hi cssAttr ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi cssProp ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi cssBoxProp ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi cssTextProp ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi cssValueLength ctermfg=141 ctermbg=NONE cterm=NONE guifg=#AF87FF guibg=NONE gui=NONE
hi cssCommonAttr ctermfg=81 ctermbg=NONE cterm=NONE guifg=#5FD7FF guibg=NONE gui=NONE
hi cssBraces ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
